/*
 * LED Blink via Potentiometer
 * @Author: Lewis Smith (abtekk)
 * 
 * Description:
 * This will blink an LED, and change the speed depending on
 * the output from a potentiometer.
 */

byte potPin = 2; //Change me if you move the POT
byte ledPin = 13; //Change me if you move the LED
int potVal = 0; //This is defined here to store the pot's value
byte brightness = 255; //Default brightness

void setup() {
  pinMode(ledPin, OUTPUT); //LED output
}

void loop() {
  potVal = analogRead(potPin);
  digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(potVal);                     //Use the pot output to set the delay
  digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
  delay(potVal); //Use the pot output to set the delay
} //End loop
