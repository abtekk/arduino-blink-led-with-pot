# ARDUINO - Blink LED with Pot

/*
 * LED Blink via Potentiometer
 * @Author: Lewis Smith (abtekk)
 * 
 * Description:
 * This will blink an LED, and change the speed depending on
 * the output from a potentiometer.
 */